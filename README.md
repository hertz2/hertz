# Hertz

## Overview
This project is a Metronome and Tuner application built using Haskell and GTK, leveraging the Euterpea library. The application provides musicians with a reliable tool for practice, featuring a metronome for keeping time and a tuner for ensuring accurate pitch.

## Team
- **[Gargi Supe](https://www.linkedin.com/in/gargi-supe-372866210/?originalSubdomain=in)**
- **[Nishita Katepallewar](https://www.linkedin.com/in/nishitakatepallewar/)**

## Features
- **Metronome**:
  - Increment or decrement the BPM (Beats Per Minute).
  - Set the desired time signature.
- **Tuner**:
  - Play a note on your instrument.
  - Observe the visual feedback to adjust your instrument's tuning.
- **User-Friendly Interface**: Built using GTK for an intuitive and accessible user experience.
- **Cross OS Compatibility**: Compatible with Linux and macOS.

## Installation

### Prerequisites
- **Haskell Platform**: Ensure you have GHC and Cabal installed.
- **GTK**: The GTK libraries must be installed on your system.
- **Euterpea Library**: This Haskell library is required for musical functionalities.
- **PyAudio**: PyAudio provides Python bindings for PortAudio v19, the cross-platform audio I/O library.
- **Fluidsynth**: Install this free open source software synthesiser.

### Steps
1. **Clone the repository**:
    ```sh
    git clone https://gitlab.com/hertz2/hertz
    cd hertzApp
    ```

2. **Install dependencies**:
    Install Euterpea (refer https://www.euterpea.com)

3. **Setup and run Fluidsynth**:
    Run Fluidsynth in an adjacent tab.
    
4. **Change Device ID**:
    Check your Fluidsynth DeviceID.
    ```sh
    ghci
    >import Euterpea
    >devices
    ```
    For Example:
    ```sh
    ghci> devices

    Input devices:
        InputDeviceID 0	IAC Driver PortMidi Output
        InputDeviceID 1	IAC Driver PortMidi Input

    Output devices:
        OutputDeviceID 2	IAC Driver PortMidi Output
        OutputDeviceID 3	IAC Driver PortMidi Input
        OutputDeviceID 4	FluidSynth virtual port (80009)
    ```
    In this case, the DeviceID should be changed to 4.
    
    cd to hertzApp/app/
    
    Change the DeviceID in Main.hs, Tuner.hs and MetronomeLogic.hs.

5. **Build the application**:
    ```sh
    stack build
    ```

6. **Run the application**:
    ```sh
    stack run
    ```
    
## Contributing
We welcome contributions to enhance the functionality and features of this app. Please follow these steps to contribute:
1. Fork the repository.
2. Create a new branch (`git checkout -b feature-xyz`).
3. Commit your changes (`git commit -am 'Add some feature'`).
4. Push to the branch (`git push origin feature-xyz`).
5. Create a new Pull Request.


## Acknowledgements
- The Haskell and GTK communities.
- Paul Hudak and Donya Quick, the developers of the Euterpea library.
- Tanisha Katepallewar, the graphic designer for Hertz.
