{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}

module Tuner (openTunerWindow) where

import Control.Concurrent (forkIO, threadDelay)
import Control.Concurrent.STM
import Control.Monad (forever)
import Data.GI.Base
import Data.GI.Base.Signals (on)
import qualified GI.Gtk as Gtk
import qualified GI.GLib as GLib
import Data.Text (pack)
import qualified Data.Text as T
import Data.Maybe (fromMaybe)
import System.IO (Handle, hGetLine, readFile)
import System.Process (createProcess, shell, std_out, StdStream(CreatePipe))
import Text.Read (readMaybe)
import Text.Printf (printf)

pitchToNote :: Float -> String
pitchToNote pitch = noteNames !! (round (12 * logBase 2 (pitch / 440)) `mod` 12)
  where
    noteNames = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]

runPitchDetection :: (TVar String, TVar String) -> IO ()
runPitchDetection (noteDisplay, deviation) = do
  let cmd = "python3 record.py"
  (_, Just hout, _, _) <- createProcess (shell cmd) { std_out = CreatePipe }
  forkIO $ reader hout noteDisplay deviation
  return ()

reader :: Handle -> TVar String -> TVar String -> IO ()
reader h noteDisplay deviation = do
  pitchData <- hGetLine h
  case readMaybe pitchData :: Maybe Float of
    Just pitch -> do
      let note = pitchToNote pitch
      atomically $ do
        writeTVar noteDisplay $ "Detected Note: " ++ note
        writeTVar deviation $ printf "Detected Pitch: %.2f Hz" pitch
      return ()
    Nothing -> putStrLn $ "Invalid pitch data: " ++ pitchData
  reader h noteDisplay deviation

openTunerWindow :: IO ()
openTunerWindow = do
  Gtk.init Nothing

  newWin <- Gtk.windowNew Gtk.WindowTypeToplevel
  Gtk.setContainerBorderWidth newWin 10
  Gtk.setWindowTitle newWin "Tuner Screen"
  Gtk.setWindowResizable newWin True
  Gtk.setWindowDefaultWidth newWin 750
  Gtk.setWindowDefaultHeight newWin 300
  Gtk.setWindowWindowPosition newWin Gtk.WindowPositionCenter
  Gtk.windowSetDecorated newWin True

  cssProvider <- Gtk.cssProviderNew
  Gtk.cssProviderLoadFromPath cssProvider "style.css"

  screen <- Gtk.widgetGetScreen newWin
  Gtk.styleContextAddProviderForScreen screen cssProvider (fromIntegral Gtk.STYLE_PROVIDER_PRIORITY_USER)

  noteDisplayBtn <- Gtk.buttonNewWithLabel "Note Display"
  Gtk.buttonSetRelief noteDisplayBtn Gtk.ReliefStyleNone

  deviationBtn <- Gtk.buttonNewWithLabel "Pitch Display"
  Gtk.buttonSetRelief deviationBtn Gtk.ReliefStyleNone

  newGrid <- Gtk.gridNew
  Gtk.gridSetColumnSpacing newGrid 10
  Gtk.gridSetRowSpacing newGrid 10
  Gtk.gridSetColumnHomogeneous newGrid True

  comboBox <- new Gtk.ComboBoxText []

  Gtk.comboBoxTextAppendText comboBox "Desired Note"
  Gtk.comboBoxSetActive comboBox 0

  content <- readFile "notes_list.txt"
  let options = map T.pack (map (filter (/= '\r')) (lines content))
  mapM_ (Gtk.comboBoxTextAppendText comboBox) options

  on comboBox #changed $ do
    activeIndex <- Gtk.comboBoxGetActive comboBox
    if activeIndex == 0
    then return ()
    else do
      activeText <- Gtk.comboBoxTextGetActiveText comboBox
      putStrLn $ T.unpack (fromMaybe "Unknown" activeText)

  scrolledWindow <- Gtk.scrolledWindowNew (Nothing :: Maybe Gtk.Adjustment) (Nothing :: Maybe Gtk.Adjustment)
  Gtk.scrolledWindowSetPolicy scrolledWindow Gtk.PolicyTypeNever Gtk.PolicyTypeAutomatic
  Gtk.widgetSetSizeRequest scrolledWindow (-1) 50 

  Gtk.containerAdd scrolledWindow comboBox

  Gtk.gridAttach newGrid scrolledWindow  0 0 1 1
  Gtk.gridAttach newGrid noteDisplayBtn  0 1 1 1
  Gtk.gridAttach newGrid deviationBtn    0 2 1 1

  Gtk.containerAdd newWin newGrid

  on newWin #destroy $ do
    putStrLn "Back to home"
    Gtk.mainQuit

  noteDisplay <- newTVarIO "Note Display"
  deviation <- newTVarIO "Pitch Display"

  runPitchDetection (noteDisplay, deviation)

  _ <- forkIO $ forever $ do
    GLib.idleAdd GLib.PRIORITY_DEFAULT $ do
      note <- atomically $ readTVar noteDisplay
      dev <- atomically $ readTVar deviation
      Gtk.buttonSetLabel noteDisplayBtn (pack note)
      Gtk.buttonSetLabel deviationBtn (pack dev)
      return False
    threadDelay 100000  

  Gtk.widgetShowAll newWin
  Gtk.main
