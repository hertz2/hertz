{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Data.GI.Base
import Data.GI.Base.Signals (on)
import Data.IORef
import qualified GI.Gtk as Gtk
import System.Directory
import Data.Text (pack)
import MetronomeLogic (playMetronome, stopMetronome)
import qualified Tuner (openTunerWindow)
import Control.Concurrent.Async (Async)

main :: IO ()
main = do
    Gtk.init Nothing

    win <- Gtk.windowNew Gtk.WindowTypeToplevel
    Gtk.setContainerBorderWidth win 10
    Gtk.setWindowTitle win "Hertz"
    Gtk.setWindowResizable win False
    Gtk.setWindowDefaultWidth win 750
    Gtk.setWindowDefaultHeight win 225
    Gtk.setWindowWindowPosition win Gtk.WindowPositionCenter
    Gtk.windowSetDecorated win True

    cssProvider <- Gtk.cssProviderNew
    Gtk.cssProviderLoadFromPath cssProvider "style.css"

    screen <- Gtk.widgetGetScreen win
    Gtk.styleContextAddProviderForScreen screen cssProvider (fromIntegral Gtk.STYLE_PROVIDER_PRIORITY_USER)

    home <- getHomeDirectory
    img1 <- Gtk.imageNewFromFile $ home ++ "/hertz/hertzApp/metronome_icon.jpg"
    img2 <- Gtk.imageNewFromFile $ home ++ "/hertz/hertzApp/tuner_icon.png"

    label1 <- Gtk.labelNew Nothing
    Gtk.labelSetMarkup label1 "<b>Metronome</b>"

    label2 <- Gtk.labelNew Nothing
    Gtk.labelSetMarkup label2 "<b>Tuner</b>"

    btn1 <- Gtk.buttonNew
    Gtk.buttonSetRelief btn1 Gtk.ReliefStyleNone
    Gtk.buttonSetImage btn1 $ Just img1
    Gtk.widgetSetHexpand btn1 False
    metronomeRef <- newIORef Nothing 

    on btn1 #clicked $ do
        Gtk.widgetHide win
        newWin <- Gtk.windowNew Gtk.WindowTypeToplevel
        Gtk.setContainerBorderWidth newWin 10
        Gtk.setWindowTitle newWin "Metronome Screen"
        Gtk.setWindowResizable newWin False
        Gtk.setWindowDefaultWidth newWin 500
        Gtk.setWindowDefaultHeight newWin 150
        Gtk.setWindowWindowPosition newWin Gtk.WindowPositionCenter
        Gtk.windowSetDecorated newWin True

        home <- getHomeDirectory
        bpmImg <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/bpm_icon.jpg"
        _22 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/22.jpg"
        _24 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/24.jpg"
        _28 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/28.jpg"
        _32 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/32.jpg"
        _34 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/34.jpg"
        _38 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/38.jpg"
        _42 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/42.jpg"
        _44 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/44.jpg"
        _48 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/48.jpg"
        plusImg <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/plus.jpg"
        minusImg <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/minus.jpg"

        bpmRef <- newIORef 120
        timeSignatureRef <- newIORef (4, 4)
        currentMetronomeRef <- newIORef Nothing

        let updateBpmBtn bpmBtn = do
                bpm <- readIORef bpmRef
                Gtk.buttonSetLabel bpmBtn $ pack $ "BPM: " ++ show bpm

        bpmBtn <- Gtk.buttonNewWithLabel "BPM: 120"
        Gtk.buttonSetRelief bpmBtn Gtk.ReliefStyleNone
        Gtk.buttonSetImage bpmBtn $ Just bpmImg
        Gtk.widgetSetHexpand bpmBtn True

        plusBtn <- Gtk.buttonNew
        Gtk.buttonSetRelief plusBtn Gtk.ReliefStyleNone
        Gtk.buttonSetImage plusBtn $ Just plusImg
        Gtk.widgetSetHexpand plusBtn True
        on plusBtn #clicked $ do
            modifyIORef bpmRef (+1)
            updateBpmBtn bpmBtn

        minusBtn <- Gtk.buttonNew
        Gtk.buttonSetRelief minusBtn Gtk.ReliefStyleNone
        Gtk.buttonSetImage minusBtn $ Just minusImg
        Gtk.widgetSetHexpand minusBtn True
        on minusBtn #clicked $ do
            modifyIORef bpmRef (\x -> x - 1)
            updateBpmBtn bpmBtn

        let onTimeSignatureClicked numerator denominator = do
                maybeCurrentMetronome <- readIORef currentMetronomeRef
                case maybeCurrentMetronome of
                    Just thread -> stopMetronome thread
                    Nothing -> return ()
                writeIORef timeSignatureRef (numerator, denominator)
                bpm <- readIORef bpmRef
                let device = 2 -- device ID
                thread <- playMetronome bpm numerator denominator device
                writeIORef currentMetronomeRef (Just thread)

        _22Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _22Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _22Btn $ Just _22
        Gtk.widgetSetHexpand _22Btn False
        on _22Btn #clicked $ onTimeSignatureClicked 2 2

        _24Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _24Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _24Btn $ Just _24
        Gtk.widgetSetHexpand _24Btn False
        on _24Btn #clicked $ onTimeSignatureClicked 2 4

        _28Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _28Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _28Btn $ Just _28
        Gtk.widgetSetHexpand _28Btn False
        on _28Btn #clicked $ onTimeSignatureClicked 2 8

        _32Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _32Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _32Btn $ Just _32
        Gtk.widgetSetHexpand _32Btn False
        on _32Btn #clicked $ onTimeSignatureClicked 3 2

        _34Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _34Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _34Btn $ Just _34
        Gtk.widgetSetHexpand _34Btn False
        on _34Btn #clicked $ onTimeSignatureClicked 3 4

        _38Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _38Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _38Btn $ Just _38
        Gtk.widgetSetHexpand _38Btn False
        on _38Btn #clicked $ onTimeSignatureClicked 3 8

        _42Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _42Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _42Btn $ Just _42
        Gtk.widgetSetHexpand _42Btn False
        on _42Btn #clicked $ onTimeSignatureClicked 4 2

        _44Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _44Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _44Btn $ Just _44
        Gtk.widgetSetHexpand _44Btn False
        on _44Btn #clicked $ onTimeSignatureClicked 4 4            

        _48Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _48Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _48Btn $ Just _48
        Gtk.widgetSetHexpand _48Btn False
        on _48Btn #clicked $ onTimeSignatureClicked 4 8

        newGrid <- Gtk.gridNew
        Gtk.gridSetColumnSpacing newGrid 10
        Gtk.gridSetRowSpacing newGrid 10
        Gtk.gridSetColumnHomogeneous newGrid True

        Gtk.gridAttach newGrid minusBtn 0 0 1 1
        Gtk.gridAttach newGrid bpmBtn    1 0 2 1
        Gtk.gridAttach newGrid plusBtn   3 0 1 1

        Gtk.gridAttach newGrid _22Btn 0 1 1 1
        Gtk.gridAttach newGrid _24Btn 1 1 1 1
        Gtk.gridAttach newGrid _28Btn 2 1 1 1
        Gtk.gridAttach newGrid _32Btn 3 1 1 1
        Gtk.gridAttach newGrid _34Btn 0 2 1 1
        Gtk.gridAttach newGrid _38Btn 1 2 1 1
        Gtk.gridAttach newGrid _42Btn 2 2 1 1
        Gtk.gridAttach newGrid _44Btn 3 2 1 1

        Gtk.containerAdd newWin newGrid

        on newWin #destroy $ do
            maybeMetronome <- readIORef currentMetronomeRef
            case maybeMetronome of
                Just thread -> stopMetronome thread
                Nothing -> return ()
            Gtk.widgetShowAll win

        Gtk.widgetShowAll newWin

    btn2 <- Gtk.buttonNew
    Gtk.buttonSetRelief btn2 Gtk.ReliefStyleNone
    Gtk.buttonSetImage btn2 $ Just img2
    Gtk.widgetSetHexpand btn2 False
    on btn2 #clicked $ do
        Gtk.widgetHide win
        Tuner.openTunerWindow >>= \_ -> Gtk.widgetShowAll win

    grid <- Gtk.gridNew
    Gtk.gridSetColumnSpacing grid 10
    Gtk.gridSetRowSpacing grid 10
    Gtk.gridSetColumnHomogeneous grid True

    Gtk.gridAttach grid btn1     0 0 1 1
    Gtk.gridAttach grid label1   0 1 1 1
    Gtk.gridAttach grid btn2     1 0 1 1
    Gtk.gridAttach grid label2   1 1 1 1
    Gtk.containerAdd win grid

    Gtk.onWidgetDestroy win Gtk.mainQuit
    Gtk.widgetShowAll win
    Gtk.main
