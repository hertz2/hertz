module MetronomeLogic where

import Euterpea
import System.Environment (getArgs)
import Control.Concurrent (threadDelay)
import Control.Concurrent.Async (Async, async, cancel)
import Data.IORef

clickSound :: Dur -> Music Pitch
clickSound d = instrument Woodblock (c 5 d)

metronome :: Int -> Int -> Int -> Music Pitch
metronome bpm beatsPerMeasure noteValue = line $ replicate beatsPerMeasure (clickSound beatDuration)
  where
    beatDuration = (4 / fromIntegral noteValue) * qn

playMetronome :: Int -> Int -> Int -> Int -> IO (Async ())
playMetronome bpm beatsPerMeasure noteValue device = do
  let music = forever $ metronome bpm beatsPerMeasure noteValue
  async $ playDev device music

stopMetronome :: Async () -> IO ()
stopMetronome = cancel

main :: IO ()
main = do
  args <- getArgs
  case args of
    [bpmStr, numeratorStr, denominatorStr] -> do
      let bpm = read bpmStr :: Int
          beatsPerMeasure = read numeratorStr :: Int
          noteValue = read denominatorStr :: Int
          device = 2 -- device ID
      thread <- playMetronome bpm beatsPerMeasure noteValue device
      ref <- newIORef thread
      threadDelay maxBound  
    _ -> putStrLn "Usage: Metronome <bpm> <numerator> <denominator>"

-- Command to run: runghc MetronomeLogic.hs 120 4 4 {120 is the bpm and 4/4 is the timesignature}
