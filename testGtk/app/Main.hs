{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Data.GI.Base
import Data.GI.Base.Signals (on)
import Data.IORef
import qualified GI.Gtk as Gtk
import System.Directory
import Data.Text (pack)

main :: IO ()
main = do
    Gtk.init Nothing

    win <- Gtk.windowNew Gtk.WindowTypeToplevel
    Gtk.setContainerBorderWidth win 10
    Gtk.setWindowTitle win "Hertz"
    Gtk.setWindowResizable win False
    Gtk.setWindowDefaultWidth win 750
    Gtk.setWindowDefaultHeight win 225
    Gtk.setWindowWindowPosition win Gtk.WindowPositionCenter
    Gtk.windowSetDecorated win True
    
    home <- getHomeDirectory
    img1 <- Gtk.imageNewFromFile $ home ++ "/hertz/testGtk/metronome_icon.png"
    img2 <- Gtk.imageNewFromFile $ home ++ "/hertz/testGtk/tuner_icon.png"

    label1 <- Gtk.labelNew Nothing
    Gtk.labelSetMarkup label1 "<b>Metronome</b>"
    
    label2 <- Gtk.labelNew Nothing
    Gtk.labelSetMarkup label2 "<b>Tuner</b>"

    btn1 <- Gtk.buttonNew
    Gtk.buttonSetRelief btn1 Gtk.ReliefStyleNone
    Gtk.buttonSetImage btn1 $ Just img1
    Gtk.widgetSetHexpand btn1 False
    on btn1 #clicked $ do
        newWin <- Gtk.windowNew Gtk.WindowTypeToplevel
        Gtk.setContainerBorderWidth newWin 10
        Gtk.setWindowTitle newWin "Metronome Screen"
        Gtk.setWindowResizable newWin False
        Gtk.setWindowDefaultWidth newWin 750
        Gtk.setWindowDefaultHeight newWin 225
        Gtk.setWindowWindowPosition newWin Gtk.WindowPositionCenter
        Gtk.windowSetDecorated newWin True

        home <- getHomeDirectory
        bpmImg <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/bpm_icon.png"
        _22 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/22.png"
        _24 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/24.png"
        _28 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/28.png"
        _32 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/32.png"
        _34 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/34.png"
        _38 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/38.png"
        _42 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/42.png"
        _44 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/44.png"
        _48 <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/48.png"
        plusImg <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/plus.png"
        minusImg <- Gtk.imageNewFromFile $ home ++ "/hertz/metronome/icons/minus.png"

        bpmRef <- newIORef 120

        let updateBpmBtn bpmBtn = do
                bpmRef <- readIORef bpmRef
                Gtk.buttonSetLabel bpmBtn $ pack $ "BPM: " ++ show bpmRef

        bpmBtn <- Gtk.buttonNewWithLabel "BPM: 120"
        Gtk.buttonSetRelief bpmBtn Gtk.ReliefStyleNone
        Gtk.buttonSetImage bpmBtn $ Just bpmImg
        Gtk.widgetSetHexpand bpmBtn True

        plusBtn <- Gtk.buttonNew
        Gtk.buttonSetRelief plusBtn Gtk.ReliefStyleNone
        Gtk.buttonSetImage plusBtn $ Just plusImg
        Gtk.widgetSetHexpand plusBtn True
        on plusBtn #clicked $ do
            modifyIORef bpmRef (+1)
            updateBpmBtn bpmBtn

        minusBtn <- Gtk.buttonNew
        Gtk.buttonSetRelief minusBtn Gtk.ReliefStyleNone
        Gtk.buttonSetImage minusBtn $ Just minusImg
        Gtk.widgetSetHexpand minusBtn True
        on minusBtn #clicked $ do
            modifyIORef bpmRef (\x -> x - 1)
            updateBpmBtn bpmBtn

        _22Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _22Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _22Btn $ Just _22
        Gtk.widgetSetHexpand _22Btn False
        on _22Btn #clicked $ do
            putStrLn "(2,2)"

        _24Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _24Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _24Btn $ Just _24
        Gtk.widgetSetHexpand _24Btn False
        on _24Btn #clicked $ do
            putStrLn "(2,4)"

        _28Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _28Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _28Btn $ Just _28
        Gtk.widgetSetHexpand _28Btn False
        on _28Btn #clicked $ do
            putStrLn "(2,8)"

        _32Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _32Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _32Btn $ Just _32
        Gtk.widgetSetHexpand _32Btn False
        on _32Btn #clicked $ do
            putStrLn "(3,2)"

        _34Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _34Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _34Btn $ Just _34
        Gtk.widgetSetHexpand _34Btn False
        on _34Btn #clicked $ do
            putStrLn "(3,4)"

        _38Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _38Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _38Btn $ Just _38
        Gtk.widgetSetHexpand _38Btn False
        on _38Btn #clicked $ do
            putStrLn "(3,8)"

        _42Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _42Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _42Btn $ Just _42
        Gtk.widgetSetHexpand _42Btn False
        on _42Btn #clicked $ do
            putStrLn "(4,2)"

        _44Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _44Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _44Btn $ Just _44
        Gtk.widgetSetHexpand _44Btn False
        on _44Btn #clicked $ do
            putStrLn "(4,4)"            

        _48Btn <- Gtk.buttonNew
        Gtk.buttonSetRelief _48Btn Gtk.ReliefStyleNone
        Gtk.buttonSetImage _48Btn $ Just _48
        Gtk.widgetSetHexpand _48Btn False
        on _48Btn #clicked $ do
            putStrLn "(4,8)"

        newGrid <- Gtk.gridNew
        Gtk.gridSetColumnSpacing newGrid 10
        Gtk.gridSetRowSpacing newGrid 10
        Gtk.gridSetColumnHomogeneous newGrid True

        Gtk.gridAttach newGrid minusBtn 0 0 1 1
        Gtk.gridAttach newGrid bpmBtn 1 0 2 1
        Gtk.gridAttach newGrid plusBtn 3 0 1 1
        Gtk.gridAttach newGrid _22Btn 0 1 1 1
        Gtk.gridAttach newGrid _24Btn 1 1 1 1
        Gtk.gridAttach newGrid _28Btn 2 1 1 1
        Gtk.gridAttach newGrid _32Btn 3 1 1 1
        Gtk.gridAttach newGrid _34Btn 0 2 1 1
        Gtk.gridAttach newGrid _38Btn 1 2 1 1
        Gtk.gridAttach newGrid _42Btn 2 2 1 1
        Gtk.gridAttach newGrid _44Btn 3 2 1 1

        Gtk.containerAdd newWin newGrid

        Gtk.widgetShowAll newWin

    btn2 <- Gtk.buttonNew
    Gtk.buttonSetRelief btn2 Gtk.ReliefStyleNone
    Gtk.buttonSetImage btn2 $ Just img2
    Gtk.widgetSetHexpand btn2 False
    on btn2 #clicked $ do
        putStrLn "You have been rickrolled"

    grid <- Gtk.gridNew
    Gtk.gridSetColumnSpacing grid 10
    Gtk.gridSetRowSpacing grid 10
    Gtk.gridSetColumnHomogeneous grid True
    
    Gtk.gridAttach grid btn1 0 0 1 1
    Gtk.gridAttach grid label1 0 1 1 1
    Gtk.gridAttach grid btn2 1 0 1 1
    Gtk.gridAttach grid label2 1 1 1 1
    Gtk.containerAdd win grid

    Gtk.onWidgetDestroy win Gtk.mainQuit
    Gtk.widgetShowAll win
    Gtk.main