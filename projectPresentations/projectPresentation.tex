\documentclass{beamer}
\usetheme{Madrid}
\usecolortheme{dove}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color}

\definecolor{codeblue}{rgb}{0.1,0.1,0.7}
\definecolor{codegreen}{rgb}{0.1,0.5,0.1}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.6,0.1,0.6}

\lstset{
    language=Haskell,
    backgroundcolor=\color{white},
    basicstyle=\ttfamily\footnotesize,
    keywordstyle=\color{codeblue},
    stringstyle=\color{codepurple},
    commentstyle=\color{codegreen},
    morecomment=[l][\color{magenta}]{\#},
    breaklines=true,
    showspaces=false,
    showstringspaces=false,
    breakatwhitespace=true,
    numbers=left,
    numberstyle=\tiny\color{codegray},
    stepnumber=1,
    numbersep=10pt
}

 \newcommand{\bgimage}{
    \usebackgroundtemplate{\includegraphics[width=\paperwidth, height=\paperheight]{backgroung.jpeg}}
}

\title{\textbf{HERTZ}}
\subtitle{Tune and Time with Haskell Precision}
\author{Nishita Katepallewar, Gargi Supe}
\date{}

\begin{document}

\bgimage
\begin{frame}
    \bgimage
    \titlepage
\end{frame}

% \begin{frame}
%     \bgimage
%     \frametitle{Outline}
%     \tableofcontents
% \end{frame}

\section{Idea}
\bgimage
\begin{frame}
    \bgimage
    \frametitle{Idea}
    \begin{itemize}
        \item Combine our passion for music and tech
        \item Create a metronome and a tuner in Haskell
        \item Use of functional programming paradigms for music analysis
    \end{itemize}
    \vspace{1.5em}
    \centering
    \begin{minipage}{0.45\textwidth}
        \includegraphics[width=\linewidth]{violin.jpg}
    \end{minipage}
    \hfill
    \begin{minipage}{0.45\textwidth}
        \includegraphics[width=\linewidth]{guitar.jpeg}
    \end{minipage}
\end{frame}

\section{Why Haskell?}
\bgimage
\begin{frame}
    \bgimage
    \frametitle{Why Haskell?}
    \begin{itemize}
        \item The Euterpea library for music processing
        \item Concurrency and parallelism in Haskell
        \item Functional programming paradigms simplify complex problems
        \item Strong typing ensures fewer runtime errors
    \end{itemize}
\end{frame}

\section{Tech Stack}
\bgimage
\begin{frame}
    \bgimage
    \frametitle{Tech Stack}
    \begin{tabular}{|l|l|}
        \hline
        \textbf{Component} & \textbf{Technology} \\
        \hline
        Programming Language & Haskell \\
        \hline
        Audio Output & PulseAudio, Fluidsynth \\
        \hline
        Audio Input & PyAudio \\
        \hline
        Frequency Analysis & Aubio \\
        \hline
        Music Composition and Analysis & Euterpea \\
        \hline
        GUI & GTK via the gtk3 library \\
        \hline
        Package Manager & Cabal \\
        \hline
    \end{tabular}
\end{frame}

\section{Resources and References}
\bgimage
\begin{frame}
    \bgimage
    \frametitle{Resources and References}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item \href{https://tinyurl.com/HSoMBook}{Yale's Haskell School of Music}
                \item \href{https://www.euterpea.com/api/}{Euterpea Documentation}
                \item \href{https://www.fluidsynth.org/}{Fluidsynth Documentation}
            \end{itemize}
        \end{column}
        \begin{column}{0.35\textwidth}
            \includegraphics[width=\textwidth]{hsom.jpg}
        \end{column}
    \end{columns}
\end{frame}

\section{Demo}
\bgimage
\begin{frame}
    \bgimage
    \frametitle{Demo}
    \centering
    \textbf{Live demonstration of Hertz's key features}
    \vspace{2em}
    \begin{itemize}
        \item Metronome with customizable tempo
        \item Real-time pitch detection
    \end{itemize}
\end{frame}

\section{Concurrency and Multithreading in Haskell}
\bgimage
\begin{frame}
    \bgimage
    \frametitle{Concurrency and Multithreading in Haskell}
    \begin{itemize}
        \item Utilized Haskell's `async` library to manage concurrent tasks
        \item Ensured real-time performance for both tuning and metronome functionalities
    \end{itemize}
\end{frame}

\bgimage
\begin{frame}
    \bgimage
    \frametitle{Async in Action}
    \lstinputlisting[caption=]{async_example.hs}
    \vspace{1em}
\end{frame}

\lstset{
    numbers=none,
    captionpos=b,
    xleftmargin=2em, 
    xrightmargin=2em 
}

\renewcommand{\lstlistingname}{}

\section{A Mathematical Intervention}
\bgimage
\begin{frame}
    \frametitle{A Mathematical Intervention}
    \lstinputlisting[caption={pitchToNote function}, label={}] {pitchToNote.hs}
    \vspace{1em}
\end{frame}

\section{Challenges Faced}
\bgimage
\begin{frame}
    \bgimage
    \frametitle{Challenges Faced}
    \begin{itemize}
        \item Different OS $\rightarrow$ Cross-platform compatibility 
        \item Limited troubleshooting forums
        \item Integrating multiple libraries
        \item Configuring hardware with code 
    \end{itemize}
    \vfill
    \centering
    \parbox{\textwidth}{
        \centering Murphy's Law
    }
    \vspace{1em}
    \vspace{0.5em}
    \vbox{
        \begin{minipage}{0.45\textwidth}
            \includegraphics[width=\linewidth]{euterpea_error.png}
            \centering \scriptsize \textit{The Curious Case of Euterpea}
        \end{minipage}
        \hfill
        \begin{minipage}{0.5\textwidth}
            \includegraphics[width=\linewidth]{midi_error.png}
            \centering \scriptsize \textit{Alas, no midi outputs!}
        \end{minipage}
    }
\end{frame}



\section{A Forking Breakthrough}
\bgimage
\begin{frame}
    \bgimage
    \frametitle{A Forking Breakthrough}
    \begin{itemize}
        \item Ensuring cross-OS compatibility at every step
        \item PortMidi + MacOs = :(
        \item Pull requests got no response
        \item Forking was something new
    \end{itemize}
\end{frame}

\section{PyAudio: A Wild Card}
\bgimage
\begin{frame}
    \bgimage
    \frametitle{PyAudio: A Wild Card}
    \lstinputlisting[label={lst:audioInput}]{audioInput.py}
    \vspace{1em}
\end{frame}

\section{Learnings}
\bgimage
\begin{frame}
    \bgimage
    \frametitle{Learnings}
    \begin{itemize}
        \item Error handling and troubleshooting
        \item Reading documentation to grasp new concepts
        \item Implementing concurrency in Haskell
        \item Configuring MIDI devices
        \item Utilizing different libraries and languages to solve a problem
        \item Giving up is never the answer!
    \end{itemize}
    \vspace{1em} 
    \centering \
    \includegraphics[width=0.45\textwidth]{givingUp.png}
\end{frame}

\bgimage
\begin{frame}
    \bgimage
    \begin{center}
        \Huge
        \textbf{SURPRISE!}
        \vspace{1cm} 
        \Large
        \par
        With our instruments now perfectly tuned, \\
        here's a special performance.
    \end{center}
\end{frame}

\end{document}

