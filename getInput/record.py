import pyaudio
import aubio
import numpy as np
import sys
import signal

BUFFER_SIZE = 256  
HOP_SIZE = 64  
CHANNELS = 1
RATE = 44100

pDetection = aubio.pitch("default", BUFFER_SIZE, HOP_SIZE, RATE)
pDetection.set_unit("Hz")
pDetection.set_silence(-40)

p = pyaudio.PyAudio()

stream = p.open(format=pyaudio.paFloat32,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=HOP_SIZE)  

print("Listening...")

def handle_exit(signal, frame):
    stream.stop_stream()
    stream.close()
    p.terminate()
    sys.exit(0)

signal.signal(signal.SIGINT, handle_exit)

try:
    while True:
        data = stream.read(HOP_SIZE)
        samples = np.frombuffer(data, dtype=aubio.float_type)
        pitch = pDetection(samples)[0]
        if pitch:
            try:
                print(f"{pitch:.2f}")
                sys.stdout.flush()
            except BrokenPipeError:
                handle_exit(None, None)
except KeyboardInterrupt:
    handle_exit(None, None)
finally:
    handle_exit(None, None)
