module Main where

import Euterpea
import Control.Concurrent (forkIO, threadDelay)
import Control.Monad (forever)
import Data.IORef (newIORef, readIORef, writeIORef)
import Data.Maybe (fromMaybe)
import Data.Text (Text, unpack)
import qualified Data.Text as T
import qualified GI.Gtk as Gtk
import Sound.PortAudio

noteToFreq :: Pitch -> Double
noteToFreq p = freq p
  where
    freq (pc, oct) = 440.0 * (2 ** (fromIntegral (pcToInt pc + (oct - 4) * 12) / 12.0))
    pcToInt pc = case pc of
      C  -> 0; Cs -> 1; D  -> 2; Ds -> 3; E  -> 4; F  -> 5
      Fs -> 6; G  -> 7; Gs -> 8; A  -> 9; As -> 10; B -> 11

captureAudio :: IO Double
captureAudio = do
  return 440.0  -- Example A4

main :: IO ()
main = do
  _ <- Gtk.init Nothing
  window <- Gtk.windowNew Gtk.WindowTypeToplevel
  Gtk.setWindowTitle window "Haskell Tuner"
  Gtk.setWindowDefaultSize window 400 200

  vbox <- Gtk.boxNew Gtk.OrientationVertical 0
  Gtk.containerAdd window vbox

  entry <- Gtk.entryNew
  Gtk.boxPackStart vbox entry False False 0

  deviationLabel <- Gtk.labelNew (Nothing :: Maybe Text)
  Gtk.boxPackStart vbox deviationLabel False False 0

  button <- Gtk.buttonNewWithLabel "Start Tuning"
  Gtk.boxPackStart vbox button False False 0

  freqRef <- newIORef 440.0  -- Default to A4 (440 Hz)

  Gtk.onButtonClicked button $ do
    text <- Gtk.entryGetText entry
    let noteStr = unpack text
    let targetNote = read noteStr :: Pitch
    let targetFreq = noteToFreq targetNote
    writeIORef freqRef targetFreq
    putStrLn $ "Target Note: " ++ show targetNote
    putStrLn $ "Target Frequency: " ++ show targetFreq

    _ <- forkIO $ do
      _ <- initialize
      putStrLn "Listening for audio input..."
      forever $ do
        inputFreq <- captureAudio
        targetFreq <- readIORef freqRef
        let deviation = inputFreq - targetFreq
        putStrLn $ "Input Frequency: " ++ show inputFreq
        putStrLn $ "Deviation from target note: " ++ show deviation ++ " Hz"
        Gtk.postGUIAsync $ Gtk.labelSetText deviationLabel (Just (T.pack $ "Deviation: " ++ show deviation ++ " Hz"))
        threadDelay 500000
      terminate

    return ()

  Gtk.onWidgetDestroy window Gtk.mainQuit
  Gtk.widgetShowAll window
  Gtk.main

