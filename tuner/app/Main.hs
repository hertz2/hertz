{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Data.GI.Base
import Data.GI.Base.Signals (on)
import qualified GI.Gtk as Gtk
import Data.Text (pack)
import qualified Data.Text as T
import Data.Maybe (fromMaybe)
import System.IO (readFile)

main :: IO ()
main = do
    Gtk.init Nothing

    newWin <- Gtk.windowNew Gtk.WindowTypeToplevel
    Gtk.setContainerBorderWidth newWin 10
    Gtk.setWindowTitle newWin "Tuner Screen"
    Gtk.setWindowResizable newWin True
    Gtk.setWindowDefaultWidth newWin 750
    Gtk.setWindowDefaultHeight newWin 300
    Gtk.setWindowWindowPosition newWin Gtk.WindowPositionCenter
    Gtk.windowSetDecorated newWin True

    cssProvider <- Gtk.cssProviderNew
    Gtk.cssProviderLoadFromPath cssProvider "style.css"

    screen <- Gtk.widgetGetScreen newWin
    Gtk.styleContextAddProviderForScreen screen cssProvider (fromIntegral Gtk.STYLE_PROVIDER_PRIORITY_USER)

    noteDisplayBtn <- Gtk.buttonNewWithLabel "Note Display"
    Gtk.buttonSetRelief noteDisplayBtn Gtk.ReliefStyleNone

    deviationBtn <- Gtk.buttonNewWithLabel "Deviation"
    Gtk.buttonSetRelief deviationBtn Gtk.ReliefStyleNone

    newGrid <- Gtk.gridNew
    Gtk.gridSetColumnSpacing newGrid 10
    Gtk.gridSetRowSpacing newGrid 10
    Gtk.gridSetColumnHomogeneous newGrid True

    comboBox <- new Gtk.ComboBoxText []

    Gtk.comboBoxTextAppendText comboBox "Desired Note"
    Gtk.comboBoxSetActive comboBox 0

    content <- readFile "notes_list.txt"
    let options = map T.pack (map (filter (/= '\r')) (lines content))
    mapM_ (Gtk.comboBoxTextAppendText comboBox) options

    on comboBox #changed $ do
        activeIndex <- Gtk.comboBoxGetActive comboBox
        if activeIndex == 0
        then return ()
        else do
            activeText <- Gtk.comboBoxTextGetActiveText comboBox
            putStrLn $ T.unpack (fromMaybe "Unknown" activeText)

    scrolledWindow <- Gtk.scrolledWindowNew (Nothing :: Maybe Gtk.Adjustment) (Nothing :: Maybe Gtk.Adjustment)
    Gtk.scrolledWindowSetPolicy scrolledWindow Gtk.PolicyTypeNever Gtk.PolicyTypeAutomatic
    Gtk.widgetSetSizeRequest scrolledWindow (-1) 50 

    Gtk.containerAdd scrolledWindow comboBox

    Gtk.gridAttach newGrid scrolledWindow  0 0 1 1
    Gtk.gridAttach newGrid noteDisplayBtn  0 1 1 1
    Gtk.gridAttach newGrid deviationBtn    0 2 1 1

    Gtk.containerAdd newWin newGrid

    on newWin #destroy $ do
        putStrLn "Back to home"
        Gtk.mainQuit

    Gtk.widgetShowAll newWin
    Gtk.main
