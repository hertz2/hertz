import Euterpea

beat :: Music PercussionSound
beat = perc AcousticSnare qn

main :: IO ()
main = play beat
